package service

import (
	"encoding/binary"
	"errors"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"pwalbc/fizzbuzz"
	"strconv"
	"sync"
	"time"

	"github.com/prologic/bitcask"
)

// Db is the key-values database that stores the query statistics
var Db *bitcask.Bitcask

// Avoid collisions when updating the stats, otherwise the count was false
var mux sync.Mutex

// Path to key-value database
const dbPath string = "./pwalbc.db"

var mostUsedKey = []byte("most_used")

// fizzBuzzHandler handles fizzbuzz requests, handles arguments and errors, launch calculation and dispatch result
func fizzBuzzHandler(w http.ResponseWriter, r *http.Request) {
	start := time.Now()

	// To allow testing with CORS requests
	w.Header().Add("Content-Type", "text/plain")
	w.Header().Add("Access-Control-Allow-Origin", "*")

	// Query parameters control
	query := fizzbuzz.Query{}
	var err error
	isOk := true

	query.Int1, err = getIntParam(r, "int1")
	if err != nil {
		isOk = false
		http.Error(w, err.Error(), http.StatusBadRequest)
		log.Println(err)
	}

	query.Str1, err = getStrParam(r, "str1")
	if err != nil {
		isOk = false
		http.Error(w, err.Error(), http.StatusBadRequest)
		log.Println(err)
	}

	query.Int2, err = getIntParam(r, "int2")
	if err != nil {
		isOk = false
		http.Error(w, err.Error(), http.StatusBadRequest)
		log.Println(err)
	}

	query.Str2, err = getStrParam(r, "str2")
	if err != nil {
		isOk = false
		http.Error(w, err.Error(), http.StatusBadRequest)
		log.Println(err)
	}

	query.Limit, err = getIntParam(r, "limit")
	if err != nil {
		isOk = false
		http.Error(w, err.Error(), http.StatusBadRequest)
		log.Println(err)
	}

	if isOk {
		fizzbuzz.Run(query, w)

		// Avoid collisions when updating the stats
		mux.Lock()
		defer mux.Unlock()

		// Get previous most used query count
		_, maxCount, _ := getMaxCount()
		// No error handling as count will be zero in case of error

		// Update current query count
		queryKeyStr := "int1=" + strconv.Itoa(query.Int1) + ",int2=" + strconv.Itoa(query.Int2) + ",str1=" + query.Str1 + ",str2=" + query.Str2 + ",limit=" + strconv.Itoa(query.Limit)
		queryKey := []byte(queryKeyStr)
		queryCount, _ := Db.Get(queryKey)
		var queryCountInt int
		if queryCount == nil {
			queryCountInt = 1
		} else {
			queryCountInt = bytetoi(queryCount) + 1
		}
		Db.Put(queryKey, itobyte(queryCountInt))

		// If current query is most used, update most used index
		isMax := queryCountInt > bytetoi(maxCount)
		if isMax {
			Db.Put(mostUsedKey, queryKey)
		}
		elapsed := time.Since(start)
		isMaxStr := ""
		if isMax {
			isMaxStr = "(is max) "
		}
		log.Printf("%s cnt:%d %stook %s", queryKeyStr, queryCountInt, isMaxStr, elapsed)
	}
}

// Get Http parameters as string values
func getStrParam(r *http.Request, name string) (string, error) {
	values, ok := r.URL.Query()[name]

	if !ok || len(values[0]) < 1 {
		return "0", errors.New("Url Param \"" + name + "\" is missing")
	}

	return values[0], nil
}

// Get Http parameters as int values
func getIntParam(r *http.Request, name string) (int, error) {
	value, err := getStrParam(r, name)

	if err != nil {
		return 0, err
	}

	return strconv.Atoi(value)
}

// Return the query with maximum count
func getMaxCount() ([]byte, []byte, error) {
	mostUsed, _ := Db.Get(mostUsedKey)
	if mostUsed == nil {
		return []byte{}, []byte{}, errors.New("No most used query")
	}

	count, _ := Db.Get(mostUsed)
	if count == nil {
		return []byte{}, []byte{}, errors.New("Most used count not found")
	}
	return mostUsed, count, nil
}

// Integer to byte array
func itobyte(num int) []byte {
	buf := make([]byte, binary.MaxVarintLen64)
	n := binary.PutVarint(buf, int64(num))
	return buf[:n]
}

// Byte array to integer
func bytetoi(bytArr []byte) int {
	x, _ := binary.Varint(bytArr)
	return int(x)
}

// statsHandler eanable to clear stats and query most used query
func statsHandler(w http.ResponseWriter, r *http.Request) {
	start := time.Now()

	// To allow testing with CORS requests
	w.Header().Add("Content-Type", "text/plain")
	w.Header().Add("Access-Control-Allow-Origin", "*")
	w.Header().Add("Access-Control-Allow-Methods", "DELETE, GET, OPTIONS")

	// Options request for CORS
	if r.Method == http.MethodOptions {
		elapsed := time.Since(start)
		log.Printf("stat option query took %s", elapsed)
		return
	}

	// Delete DB
	if r.Method == http.MethodDelete {
		Db.Close()

		var err = os.RemoveAll(dbPath)
		if err != nil {
			http.Error(w, "DB cleaning error: "+err.Error(), http.StatusInternalServerError)
		}

		Db, _ = bitcask.Open(dbPath)
		io.WriteString(w, "DB cleaned")
		elapsed := time.Since(start)
		log.Printf("DB cleaning took %s", elapsed)
		return
	}

	// Get most used query
	mostUsed, count, err := getMaxCount()
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}

	fmt.Fprintf(w, "%s : %d", string(mostUsed), bytetoi(count))
	elapsed := time.Since(start)
	log.Printf("stat query took %s", elapsed)
}

// StartServer creates the routes and starts the server
func StartServer(port string) {
	Db, _ = bitcask.Open(dbPath)
	defer Db.Close()

	http.HandleFunc("/fizzbuzz", fizzBuzzHandler)
	http.HandleFunc("/stats", statsHandler)
	log.Fatal(http.ListenAndServe(":"+port, nil))
}
