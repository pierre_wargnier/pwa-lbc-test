package main

// The main package only start the service with the right arguments from the command line interface.
import (
	"fmt"
	"os"
	"pwalbc/service"
)

func main() {
	nb := len(os.Args)
	if nb == 1 {
		fmt.Println("PWA-LBC server listening on port 8083")
		service.StartServer("8083")
	} else if nb == 2 {
		fmt.Printf("PWA-LBC server listening on port %s", os.Args[1])
		service.StartServer(os.Args[1])
	} else {
		fmt.Println("PWA-LBC command line usage: pwalbc [port]")
	}
}
