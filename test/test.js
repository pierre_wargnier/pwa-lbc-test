"use strict";

const Commons = {
	url_base: 'http://localhost:8083',
	dice: function (max) {
		return Math.ceil(max * Math.random());
	},

	// Query server
	queryUrl: function (url, assert, qMethod) {
		return new Promise((accept, reject) => {
			fetch(Commons.url_base + url, { mode: 'cors', method: qMethod ? qMethod : 'get' })
				.then(response => {
					if (!response.ok)
						reject();
					return response.blob();
				}).then(blob => {
					let reader = new FileReader();
					reader.onload = function (event) { accept(reader.result) };
					reader.readAsText(blob);
				}).catch(response => {
					assert.ok(false, "Server query error (" + response + ")");
					reject();
				});
		});
	},
};

QUnit.test("Simple query", function (assert) {

	var done = assert.async();

	Commons.queryUrl("/fizzbuzz?int1=3&int2=5&str1=fizz&str2=buzz&limit=30", assert)
		.then((result) => {
			assert.equal(result, "1,2,fizz,4,buzz,fizz,7,8,fizz,buzz,11,fizz,13,14,fizzbuzz,16,17,fizz,19,buzz,fizz,22,23,fizz,buzz,26,fizz,28,29", "Correct server response");
			done();
		}).catch(function () {
			done();
		});
});

QUnit.test("Bad query with no int1", function (assert) {

	var done = assert.async();

	Commons.queryUrl("/fizzbuzz?int2=5&str1=fizz&str2=buzz&limit=30", assert)
		.then((result) => {
			assert.ok(false, "Responded OK to query with no int1");
			done();
		}, (result) => {
			assert.ok(true, "Responded KO to query with no int1");
			done();
		});
});

QUnit.test("Bad query with no int2", function (assert) {

	var done = assert.async();

	Commons.queryUrl("/fizzbuzz?int1=3&str1=fizz&str2=buzz&limit=30", assert)
		.then((result) => {
			assert.ok(false, "Responded OK");
			done();
		}, (result) => {
			assert.ok(true, "Responded KO");
			done();
		});
});

QUnit.test("Bad query with no str1", function (assert) {

	var done = assert.async();

	Commons.queryUrl("/fizzbuzz?int1=3&int2=5&str2=buzz&limit=30", assert)
		.then((result) => {
			assert.ok(false, "Responded OK");
			done();
		}, (result) => {
			assert.ok(true, "Responded KO");
			done();
		});
});

QUnit.test("Bad query with no str2", function (assert) {

	var done = assert.async();

	Commons.queryUrl("/fizzbuzz?int1=3&int2=5&str1=fizz&limit=30", assert)
		.then((result) => {
			assert.ok(false, "Responded OK");
			done();
		}, (result) => {
			assert.ok(true, "Responded KO");
			done();
		});
});

QUnit.test("Bad query with no limit", function (assert) {

	var done = assert.async();

	Commons.queryUrl("/fizzbuzz?int1=3&int2=5&str1=fizz&str2=buzz", assert)
		.then((result) => {
			assert.ok(false, "Responded OK");
			done();
		}, (result) => {
			assert.ok(true, "Responded KO");
			done();
		});
});

QUnit.test("Stats gathering and querying", function (assert) {

	let done = assert.async(),
		limitA = (30 + Commons.dice(80)),
		limitB = (Commons.dice(30)),
		urlA = "/fizzbuzz?int1=5&int2=7&str1=fizz&str2=buzz&limit=" + limitA,
		urlB = "/fizzbuzz?int1=3&int2=5&str1=fizz&str2=buzz&limit=" + limitB,
		qAcount = 500,
		qBcount = qAcount + 200;

	Commons.queryUrl("/stats", assert, "delete") // Clear stats
		.then(() => {

			// Batch of queries
			let arr = [];
			for (let i = 0; i < qAcount; i++) {
				arr.push(Commons.queryUrl(urlA, assert))
			}
			for (let i = 0; i < qBcount; i++) {
				arr.push(Commons.queryUrl(urlB, assert))
			}

			return Promise.all(arr);
		}).then(() => {
			assert.ok(true, "Queries done");

			// Get stats
			return Commons.queryUrl("/stats", assert);
		}).then((result) => {

			// Check stats
			assert.equal(result, "int1=3,int2=5,str1=fizz,str2=buzz,limit=" + limitB + " : " + qBcount, "Stats returned correct");
			done();
		}).catch(() => {
			done();
		});
});
