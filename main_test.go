package main

import (
	"fmt"
	"os"
	"pwalbc/fizzbuzz"
	"pwalbc/service"
	"testing"
)

// StdWriter used for logging
type StdWriter struct {
}

func (sw StdWriter) Write(msg2 []byte) (n int, err error) {
	var msg3 = string(msg2[:])
	return os.Stderr.Write([]byte(msg3))
}

func BenchmarkFunction(b *testing.B) {
	logger := StdWriter{}
	query := fizzbuzz.Query{Int1: 3, Int2: 5, Str1: "fizz", Str2: "buzz", Limit: 100}
	for i := 0; i < b.N; i++ {
		fizzbuzz.Run(query, logger)
	}
}

func TestLogic(t *testing.T) {

	fmt.Println("Start FizzBuzz testing")

	br := testing.Benchmark(BenchmarkFunction)
	fmt.Println(br)
}

func TestServer(t *testing.T) {

	fmt.Println("Start FizzBuzz server")
	service.StartServer("8083")
}
