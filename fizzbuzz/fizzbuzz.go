package fizzbuzz

import (
	"fmt"
	"io"
	"strconv"
)

// Query parameters for the Fizzbuzz calculation
type Query struct {
	Int1  int
	Str1  string
	Int2  int
	Str2  string
	Limit int
}

// Run is actually doing the calculation based on Query parameter and output to Writer
func Run(query Query, writer io.Writer) {

	// We use counters and not modulos because additions are cheaper than divisions
	var cnt1, cnt2 int = 0, 0
	for i := 1; i < query.Limit; i++ {
		toPrint := ""

		cnt1++
		if cnt1 == query.Int1 {
			cnt1 = 0
			toPrint += query.Str1
		}

		cnt2++
		if cnt2 == query.Int2 {
			cnt2 = 0
			toPrint += query.Str2
		}

		if len(toPrint) == 0 {
			toPrint = strconv.Itoa(i)
		}
		fmt.Fprint(writer, toPrint)
		if i < query.Limit-1 {
			fmt.Fprint(writer, ",")
		}
	}
}
