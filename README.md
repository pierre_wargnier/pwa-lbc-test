# lbc-test

This service in implemented by Pierre Wargnier as a coding test. The goal is to create a FizzBuzz webservice that fullfills the needs stated in the requirements.txt file.

## Usage

The service is launched with the command:  
`pwalbc [port]`  
The port is 8083 by default.

Here are the possible web-service routes.

### GET /fizzbuzz

Returns a suite of integers up to a limit with integers that are modulos of two integers replaced by given words.

Query string parameters:

- int1: first modulo parameter
- int2: second modulo parameter
- str1: first modulo replacement
- str2: second modulo replacement
- limit: serie limit

### GET /stats

Returns the most common asked set of parameters with the actual query count.

### DELETE /stats

Resets the query counts.

## Tests

The Fizzbuzz logic is tested by the main_test.go file.
The service is tested by a QUnit test suite. For this just open the test/test.html in a browser.

## Structure

The program is structured in 2 packages:

- The fizzbuzz package contains the business logic.
- The service package the web service and the statistics, and depends on the fizzbuzz package.

The stats are kept in a key/value database stored in a pawlbc.db directory creaded by the server.

## Algorithm

The FizzBuzz logic uses pointers and additions. An alternative algorithm use modulo operations. Tests shows that modulo operation are slightly longer (+15%) but the response times are less stables (variation + 80%).

## Possible improvements

- Modulate the verbosity of the logs.
